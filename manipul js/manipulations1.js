// Найти параграф и получить его текстовое содержимое (только текст!)
const paragraph = document.querySelector('p');
const text = paragraph.textContent;

console.log(paragraph, text);

// Создать функцию, которая принимает в качестве аргумента узел DOM и возвращает информацию (в виде объекта) о типе узла, об имени узла и о количестве дочерних узлов (если детей нет - 0).
function domInfo(el) {
  return {
    type: el.nodeType,
    name: el.nodeName,
    val: +el.nodeValue
  };
}

console.log(domInfo(document));

// Получить массив, который состоит из текстового содержимого ссылок внутри списка: getTextFromUl(ul) ---> ["Link1", "Link2", "Link3"]

const list = document.querySelectorAll('li>a');

for (let links of list) {
  const res = links.textContent;
  console.log(res);
}

// В параграфе заменить все дочерние текстовые узлы на “-text-” (вложенные теги должны остаться). Конечный результат:
// -text-<a href="#">reprehendunt</a>-text-<mark>nemore</mark>-text-

const par = document.querySelector('p').childNodes;
for (let item of par) {
  if (item.nodeType === 3) {
    item.textContent = '-text-';
  }

  console.log(item);
}
