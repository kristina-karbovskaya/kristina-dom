// Не используя innerHTML, добавить в список несколько li с классом ‘new-item’ и текстом ‘item’ + номер li:
// <ul>
// <li><a href="#">Link1</a></li>
// <li class=”new-item”>item 5</li>
// <li class=”new-item”>item 6</li>
// </ul>

// const ul = document.querySelector('ul');
// const fragment = document.createDocumentFragment();
// const newLi = ['li', 'li'];
// newLi.forEach(li => {
//   const item = document.createElement('li');
//   item.classList.add('new-item');
//   item.textContent = `item ${li.length}`;
//   fragment.appendChild(item);
// });

// const result = ul.appendChild(fragment);
// console.log(result);

const ul = document.querySelector('ul');
const counts = 3;
const totalCounts = ul.children.length + counts;

for (let i = ul.children.length; i < totalCounts; i++) {
  const li = document.createElement('li');
  li.classList.add('new-item');
  li.textContent = `Item ${i + 1}`;
  ul.appendChild(li);
}

console.log(ul);

// В каждую ссылку, которая находятся внутри списка ul добавить по тегу strong (в каждую ссылку один - strong).
const linkUl = document.querySelectorAll('ul>li>a');

for (let a of linkUl) {
  a.innerHTML = '<strong> strong </strong>';
}

// В начало документа (в начало body) добавить картинку img с атрибутами src и alt (текст придумайте сами). В src добавьте реальный url к картинке. Для создания элемента используйте метод createElement.
const img = document.createElement('img');
img.src =
  'https://lh3.googleusercontent.com/proxy/LGj2GpkYezOP7TGRQWYObO8ZmkrWtPNPJ0Rbtd4TCH1Kvd88hxWz1miazX1DS-v0dIgzegJOFBQpCZO5DAbhfXyvYI5nrPTbamdBB-M0LN2GQAY';
img.alt = 'cat';

console.log(document.body.appendChild(img));

// Найти на странице элемент mark, добавить в конец содержимого текст “green” и на элемент установить класс green
const mark = document.querySelector('mark');
mark.textContent += ' green';
mark.classList.add('green');

console.log(mark);

// Отсортировать li внутри списка в обратном порядке (по тексту внутри)
const ulReverse = Array.from(document.querySelector('ul').children).reverse();

console.log(ulReverse);
