// Найти в коде список ul и добавить класс “list”
const ul = document.querySelector('ul');
ul.classList.add('list');
console.log(ul);

// Найти в коде ссылку, находящуюся после списка ul, и добавить id=link
const links = document.querySelectorAll('a');

for (let keys of links) {
  if (keys.parentNode === 'body') {
    keys.setAttribute('id', 'link');
  }
  console.log(keys);
}

// const [...customLinks] = links;
// customLinks.forEach(keys => {
//   if (links.parentElement === 'body') {
//     keys.setAttribute('id', 'link');
//   }
//   console.log(keys);
// });

// На li через один (начиная с самого первого) установить класс “item”
// const li = document.querySelectorAll('li');
// for (let val of li) {
//   if (val.length % 2 === 0) {
//     num.classList.add('item');
//   }
//   console.log(val);
// }

// На все ссылки в примере установить класс “custom-link”
const [...customLinks] = links;
customLinks.forEach(link => {
  link.classList.add('custom-link');
});
//
