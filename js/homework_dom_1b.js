// Создать функцию, которая принимает два элемента. Функция проверяет, является ли первый элемент родителем для второго:
// isParent(parent, child);
// isParent(document.body.children[0], document.querySelector('mark'));
//// true так как первый див является родительским элементом для mark
// isParent(document.querySelector('ul'), document.querySelector('mark'));
//// false так ul НЕ является родительским элементом для mark
// Функция принимает только DOM объекты. Обязательно проверяйте это.
function isParent(parent, child) {
  if (parent instanceof Element && child instanceof Element) {
    return parent == child.parentElement;
  }
}

console.log(
  isParent(document.querySelector('ul'), document.querySelector('li'))
);

// Используя разметку из предыдущего задания.
// Получить список всех ссылок, которые не находятся внутри списка ul.

for (let link of document.body.childNodes) {
  if (link !== document.querySelector('ul')) {
    console.log(link);
  }
}

// Используя разметку из предыдущего задания.
// Найти элемент, который находится перед и после списка ul.

const nextSibling = document.querySelector('ul').nextElementSibling;
const previousSibling = document.querySelector('ul').previousElementSibling;

console.log(nextSibling, previousSibling);

//либо используем - nextSibling, previousSibling и нам выводит #text
//почему если исп - getElementsByTagName, выдает undefined?
