// Зная структуру html, с помощью изученных методов получить (в консоль):
// 1. head
// 2. body
// 3. все дочерние элементы body и вывести их в консоль.
// 4. первый div и все его дочерние узлы
// а) вывести все дочерние узлы в консоль
// б) вывести в консоль все дочерние узлы, кроме первого и последнего
// Для навигации по DOM использовать методы, которые возвращают только элементы
//1
const head = document.head;
console.log(head);

//2
const body = document.body;
console.log(body);

//3
const childBody = document.body.childNodes;
console.log(childBody);

// 4
// const firstDiv = document.querySelector('div').childNodes;
// console.log(firstDiv);
// Если так, мы не можем вывести отдельно див
// Можно еще так
const firstDiv = document.querySelector('div');

//4a
const firstDivChild = firstDiv.childNodes;

console.log(firstDiv, firstDivChild);

// 4b
for (let i = 0; i < firstDivChild.length - 1; i++) {
  let res = firstDivChild[i];
  console.log(res);
}
