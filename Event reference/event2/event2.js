// Реализовать примитивный дропдаун. Изначально все dropdown-menu скрыты через класс .d-none. При клике на dropdown-item должен отображаться блок dropdown-menu который вложен именно в тот dropdown-item на котором произошел клик. При повторном клике на этот же dropdown-item блок dropdown-menuz должен закрыться. При клике на любой другой dropdown-item уже открытый dropdown-menu должен закрываться а на тот который кликнули открываться.

const dropDownItems = document.querySelectorAll('.dropdown-item');
const dropDownMenu = document.querySelectorAll('.dropdown-menu');

// 1 ВАРИАНТ, без этой фичи:"При клике на любой другой dropdown-item уже открытый dropdown-menu должен закрываться а на тот который кликнули открываться."

// for (let item of dropDownItems) {
//   item.addEventListener('click', e => {
//     const menu = e.currentTarget.querySelector('.dropdown-menu');
//     console.log(menu);

//     if (!menu) return;
//     menu.classList.toggle('d-none');
//   });
// }

// 2 ВАРИАНТ, с фичей.

let openedMenu = null;

dropDownItems.forEach(item =>
  item.addEventListener('click', function (e) {
    const menu = e.currentTarget.querySelector('.dropdown-menu');
    const isHidden = menu.classList.toggle('d-none');

    if (openedMenu && openedMenu !== menu) {
      openedMenu.classList.add('d-none');
    }

    if (!isHidden) {
      openedMenu = menu;
    }
  })
);
