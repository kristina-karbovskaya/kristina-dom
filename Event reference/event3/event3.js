// 1. При наведении на любой из блоков с классом .box все его дочерние элементы должны поменять свой фон на один из списка. ВАЖНО, только дочерние относительно блока на который навели мышь.
// Вот массив (список) рандомных цветов
// 2. Возращаете фон обратно когда пользователь уводит мышку с блока.
// 3. Добавление фона из первой части задания сделать с задержкой в 200мс. Т.е каждый последующий блок должен изменить свой фон за 200мс позже предыдущего. Например если первый блок поменял через 200мс то следующий должен поменять через 400 и т.д.

const colors = ['blue', 'orange', 'pink', 'yellow', 'green', 'gray', 'aqua'];

const randomColors = Math.floor(Math.random() * colors.length);

const container = document.querySelector('.container').children;
const boxes = document.querySelectorAll('.box');
const pause = 8000;

console.dir(boxes);

for (let element of container) {
  element.addEventListener('mouseover', callback, true);
}

for (let element of boxes) {
  element.addEventListener('mouseover', callback, true);
}

for (let element of container) {
  element.addEventListener('mouseover', callback, false);
}

for (let element of boxes) {
  element.addEventListener('mouseover', callback, false);
}

for (let element of container) {
  element.addEventListener('mouseout', callback, true);
}

for (let element of boxes) {
  element.addEventListener('mouseout', callback, true);
}

for (let element of container) {
  element.addEventListener('mouseout', callback, false);
}

for (let element of boxes) {
  element.addEventListener('mouseout', callback, false);
}

function callback(event) {
  let ms = (timeout = event.timeout + pause || 0);
  let target = event.currentTarget;

  setTimeout(function () {
    target.style.backgroundColor = `${colors[randomColors]}`;
    setTimeout(function () {
      target.style.backgroundColor = 'white';
    }, pause);
  }, ms);
}
