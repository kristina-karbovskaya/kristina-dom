// По нажатию на кнопку "btn-msg" должен появиться алерт с тем текстом который находится в атрибуте data-text у кнопки.

const btnMsg = document.getElementById('btn-msg');

btnMsg.addEventListener('click', () => {
  alert(btnMsg.getAttribute('data-text'));
});

// При наведении указателя мыши на "btn-msg", кнопка становится красной; когда указатель мыши покидает кнопку, она становится прежнего цвета. Цвет менять можно через добавление класса.

btnMsg.addEventListener('mouseenter', () => {
  btnMsg.style.background = '#FF2400';
  btnMsg.style.color = 'white';
});

btnMsg.addEventListener('mouseleave', () => {
  btnMsg.style.background = 'none';
  btnMsg.style.color = 'black';
});

// При нажатии на любой узел документа показать в элементе с id=tag имя тега нажатого элемента.

document.body.onclick = function (event) {
  document.getElementById('tag').textContent = `Tag: ${event.target.tagName}`;
};

// При нажатии на кнопку btn-generate добавлять в список ul элемент списка Li с текстом Item + порядковый номер Li по списку, т.е Item 3, Item 4 и т.д
const btnGenerate = document.getElementById('btn-generate');
const list = document.querySelector('ul');
const itemList = document.querySelectorAll('li');

let clickLength = list.children.length;

btnGenerate.addEventListener('click', () => {
  const clickItem = document.createElement('li');
  clickItem.textContent = `Item ${++clickLength}`;
  list.appendChild(clickItem);
});
